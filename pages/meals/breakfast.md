---
name: Breakfast
---
The most important meal of the day!

Served from 8–10am daily, on the ground floor of the MIRC.
