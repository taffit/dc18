---
name: Lunch
---
To keep going for the rest of the day.

Served from 12–13:30 daily, on the ground floor of the MIRC.
