---
name: Cheese and Wine Party
---
As always, Debanites from around the world bring their local cheeses,
wines, and other delicious food to share.

The details are coordinated on [the wiki](https://wiki.debconf.org/wiki/DebConf18/CheeseWineBoF)
