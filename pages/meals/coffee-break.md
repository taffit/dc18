---
name: Coffee Break
---
Not a meal per-se, but Debianites need their fuel.

Served from 15:30–16:00 daily, on the ground floor of the MIRC.
